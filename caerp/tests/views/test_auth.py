"""
    Tests
"""
from sqlalchemy import select
import datetime
from caerp.models.user.login import UserConnections


def test_redirect(app):
    login_url = "http://localhost/login?nextpage=%2F"
    resp = app.get("/")
    assert resp.status_int == 302
    assert login_url in list(dict(resp.headerlist).values())


def test_xhr_redirect(app):
    login_url = "http://localhost/login?nextpage=%2F"
    resp = app.get("/", xhr=True)
    assert resp.status_int == 200
    assert resp.json["redirect"] == login_url


def test_check_login(app, config):
    login_url = "http://localhost/api/v1/login"
    resp = app.get(login_url, xhr=True)
    assert resp.json["status"] == "error"
    assert "login_form" in resp.json["datas"]


def test_connect_user(get_csrf_request_with_db, dbsession, login):
    from caerp.views.auth.basic_views import connect_user

    request = get_csrf_request_with_db()
    connect_user(request, login.login, True)
    today = datetime.date.today()
    query = select(UserConnections).where(
        UserConnections.user_id == login.user_id,
        UserConnections.month == today.month,
        UserConnections.year == today.year,
    )
    assert dbsession.execute(query).scalar() is not None
