import datetime
from caerp.controllers.state_managers import set_validation_status


def test_number_collision_fix2975(
    mk_supplier_invoice,
    csrf_request_with_db_and_user,
):
    from caerp.models.config import Config

    Config.set("supplierinvoice_number_template", "{YY}-{MM}-{SEQMONTH}")

    supplier_invoice_1 = mk_supplier_invoice(
        status_date=datetime.date(2021, 9, 1), date=datetime.date(2021, 9, 1)
    )
    supplier_invoice_2 = mk_supplier_invoice(
        status_date=datetime.date(2021, 10, 1), date=datetime.date(2021, 9, 1)
    )

    assert supplier_invoice_1.official_number is None
    assert supplier_invoice_2.official_number is None

    set_validation_status(csrf_request_with_db_and_user, supplier_invoice_1, "valid")
    assert supplier_invoice_1.official_number == "21-09-1"

    set_validation_status(csrf_request_with_db_and_user, supplier_invoice_2, "valid")

    assert supplier_invoice_2.official_number == "21-09-2"
