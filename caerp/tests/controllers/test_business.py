import pytest
from caerp.controllers.business import gen_sold_invoice


@pytest.fixture
def classic_estimation_with_business(
    dbsession, estimation, mk_task_line, mk_task_line_group, tva
):
    line = mk_task_line(
        tva=200, cost=20000000, description="Product line"  # 200 euros HT
    )
    estimation.line_groups = [mk_task_line_group(lines=[line])]
    estimation.status = "valid"
    estimation.business.populate_deadlines()
    dbsession.merge(estimation)
    dbsession.flush()

    return estimation


def test_gen_sold_invoice(
    get_csrf_request_with_db, classic_estimation_with_business, user
):
    invoice = gen_sold_invoice(
        get_csrf_request_with_db(user=user), classic_estimation_with_business.business
    )
    assert invoice.total() == classic_estimation_with_business.total()
    assert invoice.business_id == classic_estimation_with_business.business_id


def test_gen_sold_invoice_two_times_fix_4279(
    get_csrf_request_with_db, classic_estimation_with_business, user
):
    invoice1 = gen_sold_invoice(
        get_csrf_request_with_db(user=user), classic_estimation_with_business.business
    )
    invoice2 = gen_sold_invoice(
        get_csrf_request_with_db(user=user),
        classic_estimation_with_business.business,
        ignore_previous_invoices=True,
    )
    invoice3 = gen_sold_invoice(
        get_csrf_request_with_db(user=user),
        classic_estimation_with_business.business,
        ignore_previous_invoices=True,
    )
    assert (
        invoice1.total()
        == invoice2.total()
        == invoice3.total()
        == classic_estimation_with_business.total()
    )
