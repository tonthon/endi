import io
import colander
import pytest
from caerp.utils.image import ImageResizer
from caerp.forms.files import get_file_upload_preparer, FileUploadSchema


def test_get_file_upload_preparer_issue_1148(get_csrf_request_with_db):
    # La validation du type de fichier est faite après le pré-filtrage
    # Si les fichiers déposés ne sont pas des images, ils ne sont pas
    # traités par les filtres et renvoyés tel quel
    filters = [ImageResizer(100, 100)]

    preparer = get_file_upload_preparer(filters)

    file_data = io.BytesIO()
    file_data.write(b"I'm not an image")
    file_data.seek(0)
    data = {
        "fp": file_data,
        "filename": "test.jpg",
        "mimetype": "image/jpeg",
        "size": file_data.getbuffer().nbytes,
    }
    result = preparer(data)
    assert result["fp"] == data["fp"]


class TestFileUploadSchema:
    @pytest.fixture
    def schema(self):
        return FileUploadSchema()

    def test_parent_id_missing(self, get_csrf_request_with_db, schema, invoice):
        request = get_csrf_request_with_db()
        bound_schema = schema.bind(request=request)
        assert bound_schema["parent_id"].missing == colander.required

        request.context = invoice
        bound_schema = schema.bind(request=request)
        assert bound_schema["parent_id"].missing == colander.drop
