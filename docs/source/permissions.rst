Fonctionnement des droits et permissions
==========================================

Utilisateur connecté et Principals
-----------------------------------

Lorsqu'un utilisateur est connecté, on détermine ses **principals**.

.. code-block:: python

    [
        'system.Everyone',
        'system.Authenticated',
        'prenom.nom@example.com',
        'user:1035',
        'login:269',
        'group:trainer',
        'access_right:es_trainer',
        'group:accounting',
        'access_right:global_config_accounting',
        'access_right:global_config_sale',
        'access_right:global_config_supply',
        'access_right:global_accountant',
        'access_right:global_validate_invoice',
        'access_right:global_validate_cancelinvoice',
        'access_right:global_validate_estimation',
        'access_right:global_validate_supplier_order',
        'access_right:global_validate_supplier_invoice',
        'access_right:global_validate_expensesheet',
        'access_right:global_record_payment_invoice',
        'access_right:global_record_payment_supplier_invoice',
        'access_right:global_record_payment_expensesheet',
        'access_right:global_company_supervisor',
        'access_right:global_supervisor_salary',
        'access_right:global_userdata_restricted',
        'company:181527',
        'company:181750'
    ]


Context et traversal
---------------------

En parallèle de la construction des principals d'un utlisateur,
l'application détermine un contexte pour la requête courante.
Celui-ci est:

    - un modèle
    - la :py:class:`caerp.utils.security.acls.RootFactory`


L'arbre de traversal est définit dans :py:mod:`caerp.utils.security.acls`.

Voir :doc:`pyramid_traversal`

Permission
-------------------

Les permissions sont utlisées pour contrôler :

    - L'accès à une resource (view Pyramid)

    - Les actions sur des modèles (validation d'une facture par exemple)

Les permissions sont vérifiées à l'aide de la méthode has_permission de l'objet request

.. code-block:: python

    # Check la permission context.add_file sur le contexte courant
    request.has_permission(PERMISSIONS["context.add_file"])
    # Check la permission context.add_file sur un objet particulier (autre que le contexte)
    request.has_permission(PERMISSIONS["context.add_file"], mon_instance_de_modele_sqlalchemy)

Au sein des templates, à l'aide d'un utilitaire api.has_permission

.. code-block:: python

    # Check la permission context.add_file sur le contexte courant
    api.has_permission("context.add_file")
    # Check la permission context.add_file sur un objet particulier (autre que le contexte)
    api.has_permission("context.add_file", mon_instance_de_modele_sqlalchemy)

L'ensemble des permissions sont définies dans :py:mod:`caerp.consts.permissions`


Sous le capot, Pyramid teste la permission via la :py:class:`caerp.utils.security.policy.SessionSecurityPolicy`

SessionSecurityPolicy
-----------------------

La :py:class:`caerp.utils.security.policy.SessionSecurityPolicy` traite les différentes permissions
en fonction de leur préfixe :

    - 'global.' : pour des permissions globales

    - 'company:' : pour des permissions spécifiques à une enseigne

    - Pour les autres elle délègue le traitement au système d'ACL de pyramid.


Permissions globales
......................

Les permissions globales d'un utilisateur sont déterminées via le chemin suivant :

    - :py:class:`caerp.models.user.login.Login`

    - :py:class:`caerp.models.user.group.Group`

    - :py:class:`caerp.models.user.access_right.AccessRight`

    - :py:data:`caerp.consts.access_rights`


Permissions au niveau de l'enseigne
......................................

Les permissions au niveau de l'enseigne sont déterminées en fonction du context passé à **has_permission**:

    - On récupère l'enseigne associée au contexte

    - On calcule les acl sur l'enseigne

    - Les ACL de l'enseigne sont comparées aux principals de l'utilisateur courant


Permissions au niveau d'un modèle
...................................

Les permissions au niveau des modèles sont déterminées en fonction du context passé à **has_permission**:

    - On calcule les acl sur l'instance de modèle

    - Les ACL sur l'instance de modèle sont comparées aux principals de l'utilisateur courant


Modèles et bout de code concernés
-----------------------------------

:py:class:`caerp.models.user.access_right.AccessRight`

:py:class:`caerp.models.user.group.Group`

:py:class:`caerp.models.user.user.User`

:py:class:`caerp.models.user.login.Login`

:py:class:`caerp.utils.security.policy.SessionSecurityPolicy`

:py:func:`caerp.utils.security.identity.get_identity`

:py:func:`caerp.services.company.find_company_id_from_model`

:py:mod:`caerp.consts.permissions`

:py:mod:`caerp.consts.access_rights`

