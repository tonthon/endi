Export des écritures au format Isa Compta
=======================================================

CAERP permet de configurer l'export des écritures au format compatible avec ce
qui est attendu par Isa compta.

Afin de configurer CAERP pour utiliser ces modules, la configuration suivante
doit être ajoutée dans la section "[app:caerp]" du fichier .ini

.. code-block::

    caerp.services.treasury_invoice_producer=caerp.compute.isacompta.compute.InvoiceProducer
    caerp.services.treasury_internalinvoice_producer=caerp.compute.isacompta.compute.InternalInvoiceProducer
    caerp.services.treasury_invoice_writer=caerp.export.isacompta.InvoiceWriter

    caerp.services.treasury_payment_producer=caerp.compute.isacompta.compute.PaymentProducer
    caerp.services.treasury_internalpayment_producer=caerp.compute.isacompta.compute.InternalPaymentProducer
    caerp.services.treasury_payment_writer=caerp.export.isacompta.PaymentWriter

    caerp.services.treasury_expense_producer=caerp.compute.isacompta.compute.ExpenseProducer
    caerp.services.treasury_expense_writer=caerp.export.isacompta.ExpenseWriter

    caerp.services.treasury_supplier_invoice_producer=caerp.compute.isacompta.compute.SupplierInvoiceProducer
    caerp.services.treasury_internalsupplier_invoice_producer=caerp.compute.isacompta.compute.InternalSupplierInvoiceProducer
    caerp.services.treasury_supplier_invoice_writer=caerp.export.isacompta.SupplierInvoiceWriter

Grand livre
------------

Un outil de traitement spécifique au grand livre issu d'isacompta permet
d'intégrer le grand livre directement dans caerp.

Dans la configuration du service caerp_celery ajouter

.. code-block::

    caerp_celery.interfaces.IAccountingFileParser=caerp_celery.parsers.isacompta.parser_factory
    caerp_celery.interfaces.IAccountingOperationProducer=caerp_celery.parsers.isacompta.producer_factory


Spécifications pour la configuration des imports IsaCompta
-------------------------------------------------------------

Les chapitres ci-dessous décrivent les formats des fichiers produits par CAERP
pour Isa compta. Les imports dans IsaCompta doivent être configurés en fonction
des données ci-dessous.

Import Facture Vente
......................

- Entête dans le fichier d'import : Numéro de pièce
- Contient : Numéro de facture CAERP


- Entête dans le fichier d'import : Code Journal
- Contient : code journal configuré dans l'administration d'caerp


- Entête dans le fichier d'import : Date de pièce
- Contient : Date


- Entête dans le fichier d'import : N° compte général
- Contient : Compte général


- Entête dans le fichier d'import : Code tva
- Contient : Code TVA


- Entête dans le fichier d'import : Numéro de compte tiers
- Contient : Compte Tiers


- Entête dans le fichier d'import : Libellé pièce
- Contient : Nom du client


- Entête dans le fichier d'import : Libellé mouvement
- Contient : Libellé configuré dans l'administration d'caerp


- Entête dans le fichier d'import : Montant débit
- Contient : Débit


- Entête dans le fichier d'import : Montant crédit
- Contient : Crédit


- Entête dans le fichier d'import : Numéro analytique
- Contient : Numéro analytique de l'enseigne

Import Notes de dépenses
...........................

- Entête dans le fichier d'import : Numéro de pièce
- Contient : Numéro du document dans CAERP


- Entête dans le fichier d'import : Code Journal
- Contient : code journal configuré dans l'administration d'caerp


- Entête dans le fichier d'import : Date de pièce
- Contient : Date


- Entête dans le fichier d'import : N° compte général
- Contient : Compte général


- Entête dans le fichier d'import : Code tva
- Contient : Code TVA


- Entête dans le fichier d'import : Numéro de compte tiers
- Contient : Compte Tiers


- Entête dans le fichier d'import : Libellé pièce
- Contient : Nom de l'entrepreneur


- Entête dans le fichier d'import : Libellé mouvement
- Contient : Libellé configuré dans l'administration d'caerp


- Entête dans le fichier d'import : Montant débit
- Contient : Débit


- Entête dans le fichier d'import : Montant crédit
- Contient : Crédit


- Entête dans le fichier d'import : Numéro analytique
- Contient : Numéro analytique de l'enseigne


Import des encaissements
.........................

- Entête dans le fichier d'import : Référence
- Contient : reference


- Entête dans le fichier d'import : Code Journal
- Contient : code journal configuré dans l'administration d'caerp


- Entête dans le fichier d'import : Date de pièce
- Contient : Date


- Entête dans le fichier d'import : N° compte général
- Contient : Compte général


- Entête dans le fichier d'import : Code tva
- Contient : Code TVA


- Entête dans le fichier d'import : Numéro de compte tiers
- Contient : Compte Tiers


- Entête dans le fichier d'import : Libellé pièce
- Contient : Nom du client


- Entête dans le fichier d'import : Libellé mouvement
- Contient : Libellé configuré dans l'administration d'caerp


- Entête dans le fichier d'import : Montant débit
- Contient : Débit


- Entête dans le fichier d'import : Montant crédit
- Contient : Crédit


- Entête dans le fichier d'import : Numéro analytique
- Contient : Numéro analytique de l'enseigne


Import des factures fournisseurs
.................................

- Entête dans le fichier d'import : Numéro de pièce
- Contient : Numéro du document dans CAERP


- Entête dans le fichier d'import : Code Journal
- Contient : code journal configuré dans l'administration d'caerp


- Entête dans le fichier d'import : Date de pièce
- Contient : Date


- Entête dans le fichier d'import : N° compte général
- Contient : Compte général


- Entête dans le fichier d'import : Code tva
- Contient : Code TVA


- Entête dans le fichier d'import : Numéro de compte tiers
- Contient : Compte Tiers


- Entête dans le fichier d'import : Libellé pièce
- Contient : Nom du fournisseur


- Entête dans le fichier d'import : Libellé mouvement
- Contient : Libellé configuré dans l'administration d'caerp


- Entête dans le fichier d'import : Montant débit
- Contient : Débit


- Entête dans le fichier d'import : Montant crédit
- Contient : Crédit


- Entête dans le fichier d'import : Numéro analytique
- Contient : Numéro analytique de l'enseigne
