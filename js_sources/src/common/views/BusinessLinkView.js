import Mn from 'backbone.marionette';

/** Display 3 lines with icons, describing linked Business/Project/Customer
 *
 * Linked resource correspond to those listed in the BusinessLinkedModelMixin,
 * they all are optional.
 *
 * For each linked resource, if undefined, nothing is displayed, else, an icon
 * with the resource label is displayed.
 *
 * Required options:
 * - customer_label
 * - project_label
 * - business_label
 */

const BusinessLinkView = Mn.View.extend({
    template: require("./templates/BusinessLink.mustache"),
    templateContext: function() {
        return {
            customer_label: this.getOption('customer_label'),
            project_label: this.getOption('project_label'),
            business_label: this.getOption('business_label'),
        }
    }
});
export default BusinessLinkView;
