import Radio from 'backbone.radio';
import Mn from 'backbone.marionette';



const Controller = Mn.Object.extend({
    initialize(options) {
        this.facade = Radio.channel('facade');
        this.config = Radio.channel('config');
        this.rootView = options['rootView'];
        this.chapters = options['chapters'];
    },
    index() {
        this.rootView.index();
    },
    showModal(view) {
        this.rootView.showModal(view);
    },
    editProduct(chapterId, modelId) {
        const chapterModel = this.chapters.get(chapterId);
        const collection = chapterModel.products;
        let request = collection.fetch();
        request = request.then(function () {
            let model = collection.get(modelId);
            return model;
        });
        request.then(this.rootView.showEditProductForm.bind(this.rootView));
    },
    // WorkItem 
    editWorkItem(model) {
        this.rootView.showEditProductForm(model);
    },
});
export default Controller;