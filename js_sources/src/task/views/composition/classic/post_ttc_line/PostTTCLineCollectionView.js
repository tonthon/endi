import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import PostTTCLineView from './PostTTCLineView.js';
import Validation from 'backbone-validation';

const PostTTCLineCollectionView = Mn.CollectionView.extend({
    tagName: 'tbody',
    className: 'lines',
    childView: PostTTCLineView,
    // Bubble up child view events
    childViewTriggers: {
        'edit': 'line:edit',
        'delete': 'line:delete'
    },
    childViewOptions(model){
        // Forward the edit option to the children
        return {edit: this.getOption('edit')}
    },
    initialize: function(options){
        var channel = Radio.channel('facade');
        this.listenTo(channel, 'bind:validation', this.bindValidation);
        this.listenTo(channel, 'unbind:validation', this.unbindValidation);
    },
    showErrors(model, errors){
        this.$el.addClass('error');
    },
    hideErrors(model){
        this.$el.removeClass('error');
    },
    bindValidation(){
        Validation.bind(this);
    },
    unbindValidation(){
        Validation.unbind(this);
    },
});
export default PostTTCLineCollectionView;
