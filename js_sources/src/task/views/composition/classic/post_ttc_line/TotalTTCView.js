import Mn from 'backbone.marionette';
import { formatAmount } from "../../../../../math.js";
import LabelRowWidget from '../../../../../widgets/LabelRowWidget.js';

const TotalTTCView = Mn.View.extend({
    className: 'table_container',
    template: require('./templates/TotalTTCView.mustache'),
    regions: {
        line: ".line"
    },
    modelEvents: {
        'change': 'render',
    },
    onRender: function(){
        var values = formatAmount(this.model.get('ttc'), true);
        var view = new LabelRowWidget(
            {
                label: 'Total TTC',
                values: values,
                colspan: 5
            }
        );
        this.showChildView('line', view);
    }
});
export default TotalTTCView;
