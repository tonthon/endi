import Mn from "backbone.marionette";
import FileView from "./FileView";
import {
    getOpt
} from "../../../tools";


/**
 * Collection view for file representation. 
 * The childview can be specified on init
 * 
 * @fires 'file:updated' : when a file DL popup has been closed (file added/deleted/updated)
 */
const FileCollectionView = Mn.CollectionView.extend({
    tagName: 'tbody',
    childView: FileView,
    childViewTriggers: {
        "file:updated": "file:updated"
    },
    initialize(options) {
        this.requirement_filter = getOpt(this, 'requirement_filter', null);
    },
    viewFilter(view, index, children) {
        if (this.requirement_filter) {
            return this.requirement_filter(view.model);
        }
        return view.model.get('requirement_type') != 'optionnal';
    }
});
export default FileCollectionView;