import TextAreaWidget from '../../../widgets/TextAreaWidget'
import BaseQualiopiKnowledgeSaleProductForm from './BaseQualiopiKnowledgeSaleProductForm'

/** Handle Form logic of fields from SaleProductVAE model
 *
 * Label/descriptions are kept in sync manually with python models. Take care of it <3.
 */
const VAESaleProductForm = BaseQualiopiKnowledgeSaleProductForm.extend({
    complexTitle: "Titre de la VAE",
    regions: _.defaults(
      {
        eligibility_process: '.field-eligibility_process',
      },
      BaseQualiopiKnowledgeSaleProductForm.prototype.regions
    ),
    trainingFields: _.defaults({
        eligibility_process: {
            label: 'Processus d’éligibilité et de recevabilité',
            required: true,
            widget: TextAreaWidget
        },
    }, BaseQualiopiKnowledgeSaleProductForm.prototype.trainingFields),
})

export default VAESaleProductForm
