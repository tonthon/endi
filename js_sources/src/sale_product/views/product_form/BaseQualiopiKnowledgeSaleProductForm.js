import TextAreaWidget from '../../../widgets/TextAreaWidget'
import InputWidget from '../../../widgets/InputWidget'
import BaseQualiopiSaleProductForm from './BaseQualiopiSaleProductForm'


/** Abstract class to handle Form logic of fields from BaseQualiopiKnowledgeSaleProduct
 *
 * Label/descriptions are kept in sync manually with python models. Take care of it <3.
 */
const BaseQualiopiKnowledgeSaleProductForm = BaseQualiopiSaleProductForm.extend({
    regions: _.defaults(
      {
          goals: '.field-goals',
          for_who: '.field-for_who',
          duration_days: '.field-duration_days',
          evaluation: '.field-evaluation',
          place: '.field-place'
      },
      BaseQualiopiSaleProductForm.prototype.regions
    ),
    trainingFields: _.defaults({
        goals: {
            label: 'Objectifs à atteindre à l\'issue de la formation',
            description: 'Les objectifs doivent être obligatoirement décrit avec des verbes d\'actions',
            required: true,
            widget: TextAreaWidget,
            tinymce: true,
        },
        for_who: {
            label: 'Pour qui ?',
            description: 'Public susceptible de participer à cette formation.',
            required: true,
            widget: TextAreaWidget
        },
        duration_days: {
            label: 'Durée en jours de la formation',
            required: true,
            widget: InputWidget,
            type: 'number'
        },
        evaluation: {
            label: 'Modalités d\'évaluation de la formation',
            description: 'Par exemple : questionnaire d\'évaluation, exercices-tests, questionnaire de satisfaction, évaluation formative.',
            required: true,
            widget: TextAreaWidget,
            tinymce: true,
        },
        place: {
            label: 'Lieu de la formation',
            description: 'Villes, zones géographiques où la formation peut être mise en place.',
            widget: TextAreaWidget
        }
    }, BaseQualiopiSaleProductForm.prototype.trainingFields)
})

export default BaseQualiopiKnowledgeSaleProductForm
