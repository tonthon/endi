import BaseQualiopiSaleProduct from './BaseQualiopiSaleProduct'

/** Abstract class to handle model fields of BaseQualiopiKnowledgeSaleProduct back-end model
 */
const BaseQualiopiKnowledgeSaleProduct = BaseQualiopiSaleProduct.extend({
    props: [
        "goals",
        "for_who",
        "duration_days",
        "evaluation",
        "place",
    ],
});
BaseQualiopiKnowledgeSaleProduct.prototype.props = BaseQualiopiKnowledgeSaleProduct.prototype.props.concat(BaseQualiopiSaleProduct.prototype.props);

export default BaseQualiopiKnowledgeSaleProduct;
