import getFormConfigStore from './formConfig'
import api from '@/api/index'
import { defineStore } from 'pinia'

export const useTaskConfigStore = getFormConfigStore('task')

export const useTaskStore = defineStore('task', {
  state: () => ({
    loading: true,
    error: false,
    item: {},
  }),
  actions: {
    setAddUrl(url) {
      api.tasks.setCollectionUrl(url)
    },
    async createTask(data) {
      if (data.id) {
        throw Error('Task already exists (has an id)')
      }
      return api.tasks.create(data)
    },
  },
})
