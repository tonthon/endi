import { startApp } from '@/helpers/utils'
import App from './App.vue'
import CompanyURLSearchFilters
  from '@/components/company/CompanyUrlSearchFilters.vue'

const routes = [
  {path: '/companies_map', components: { CompanyURLSearchFilters } },
]

const app = startApp(App, 'vue-app', routes)
