import http from './http'

export const collectOptions = () => AppOption

export const getCsrfToken = () => collectOptions()['csrf_token']

export const getAssetPath = (asset) => AppOption['static_path'] + asset
